FROM node:10
COPY . /app
EXPOSE 3000
RUN cd /app && npm install
WORKDIR /app
CMD node dist/main.js