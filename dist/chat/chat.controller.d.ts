import { Response, Request } from 'express';
import { AppService } from 'src/app.service';
export declare class ChatController {
    private appService;
    constructor(appService: AppService);
    auth(res: Response, req: Request): void;
    users(): number;
}
