import { AppService } from './app.service';
import { Response } from 'express';
export declare class userProperties {
    readonly email: string;
    readonly password: string;
    readonly prenom: string;
    readonly ville: string;
    readonly age: number;
    readonly expertise: string;
    readonly is_helping: boolean;
    readonly picture: string;
}
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    createUser(res: Response, logs: userProperties): Promise<void>;
}
