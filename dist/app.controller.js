"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_service_1 = require("./app.service");
const firebase = require("firebase-admin");
class userProperties {
}
exports.userProperties = userProperties;
;
let AppController = class AppController {
    constructor(appService) {
        this.appService = appService;
    }
    async createUser(res, logs) {
        let user = null;
        try {
            let { email, password } = logs, userProperties = __rest(logs, ["email", "password"]);
            let values = Object.values(userProperties);
            if (values.includes(null))
                throw { code: 400, message: "Missing field" };
            user = await firebase.auth().createUser({ email, password });
            let userCollection = firebase.firestore().collection('user');
            await userCollection.doc(user.uid).set(Object.assign({}, userProperties));
            let userId = user.uid;
            let userName = userProperties.prenom;
            await this.appService.chatkit
                .createUser({
                id: userId,
                name: userName,
            });
            res.status(common_1.HttpStatus.CREATED).send();
        }
        catch (error) {
            if (user)
                await firebase.auth().deleteUser(user.uid);
            res.status(common_1.HttpStatus.BAD_REQUEST).send(error);
        }
    }
};
__decorate([
    common_1.Post(),
    __param(0, common_1.Res()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, userProperties]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "createUser", null);
AppController = __decorate([
    common_1.Controller('user'),
    __metadata("design:paramtypes", [app_service_1.AppService])
], AppController);
exports.AppController = AppController;
//# sourceMappingURL=app.controller.js.map