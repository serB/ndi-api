"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const firebase = require("firebase-admin");
const bodyParser = require("body-parser");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.enableCors();
    await app.listen(3000);
}
bootstrap();
firebase.initializeApp({
    credential: firebase.credential.applicationDefault(),
    databaseURL: "https://ndi-2019.firebaseio.com"
});
//# sourceMappingURL=main.js.map