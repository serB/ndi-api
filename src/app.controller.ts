import { Controller, Post, Body, Get, Res, HttpStatus } from '@nestjs/common';
import { AppService } from './app.service';

import { Response } from 'express';

import * as firebase from 'firebase-admin';
import { CollectionReference } from '@google-cloud/firestore';


export class userProperties{
  readonly email: string;
  readonly password: string;
  readonly prenom: string;
  readonly ville: string;
  readonly age: number;
  readonly expertise: string;
  readonly is_helping: boolean;
  readonly picture: string;
};

@Controller('user')
export class AppController {


  constructor(private readonly appService: AppService) {}
  
  @Post()
  async createUser(@Res() res: Response, @Body() logs: userProperties) {
    
    let user = null;
    try {
      let {email, password, ...userProperties} = logs;
      let values = Object.values(userProperties);
      if(values.includes(null))
        throw {code:400, message:"Missing field"};
      user = await firebase.auth().createUser({email, password});
      let userCollection: CollectionReference = firebase.firestore().collection('user');
      await userCollection.doc(user.uid).set({...userProperties});

      let userId = user.uid;
      let userName = userProperties.prenom;
    
      await this.appService.chatkit
            .createUser({
                id: userId,
                name: userName,
            });
      res.status(HttpStatus.CREATED).send();
    } catch (error) {
      if(user)
        await firebase.auth().deleteUser(user.uid);
      res.status(HttpStatus.BAD_REQUEST).send(error);
    }
  }
}

