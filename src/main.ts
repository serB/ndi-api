import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import * as firebase from "firebase-admin";
import bodyParser = require('body-parser');

import * as Chatkit from '@pusher/chatkit-server';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
firebase.initializeApp({
  credential: firebase.credential.applicationDefault(),
  databaseURL: "https://ndi-2019.firebaseio.com"
});

