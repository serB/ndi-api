import { Injectable } from '@nestjs/common';
import * as Chatkit from '@pusher/chatkit-server';

@Injectable()
export class AppService {

  public chatkit;

  constructor(){
    this.chatkit = new Chatkit.default({
      instanceLocator: process.env.CHATKIT_INSTANCE_LOCATOR,
      key: process.env.CHATKIT_SECRET_KEY,
    });
  }
}
