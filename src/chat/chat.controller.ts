import { Controller, Post, Res, Req } from '@nestjs/common';
import { Response, Request } from 'express';
import { AppService } from 'src/app.service';

@Controller('chat')
export class ChatController {

    constructor(private appService: AppService){}

    @Post('authenticate')
    auth(@Res() res: Response, @Req() req: Request) {
        const authData = this.appService.chatkit.authenticate({
            userId: req.query.user_id,
        });
        res.status(authData.status).send(authData.body);
    }

    @Post('users')
    users() {
        return 201;
    }

}